;=====================================================================================
; Lukas Richtrmoc, 2020
; University of Hradec Kralove, Czech Republic
;
; Corresponding author contact: richtlu2@uhk.cz
;
; Project details and paper available at:
;
;   https://bitbucket.org/bohemak2/tragedy-of-the-commons
;=====================================================================================


; BEGIN NETLOGO MODEL CODE

; Set up agents
breed [consumers a-green] ; consumers, take resources per tick
breed [shops a-blue]      ; shops

globals [
  needs                ; sum of needed resources
  threshold            ; for coordinations (at home, at shop)
]


consumers-own [
  home-x ;; position of home X
  home-y ;; position of home Y
  stock-resource ;; resources on stock
  consumption ;; variable for family consumption
  load ;; how much resources willing to buy in advance
  panic ;; consumer is in panic because of lacking resources
]

shops-own [
  stock-resources ;; resources on stock
]

to setup
  clear-all
  ask patches [ set pcolor grey ]
  set-default-shape consumers "person"
  set-default-shape shops "x"
  set threshold 2

  create-consumers consumers-count [
    set color black
    set size 2.5
    set home-x random-xcor
    set home-y random-xcor
    set xcor home-x
    set ycor home-y

    ; Load represents needs of stocked resources in advance at home 1 day ... 30 days * multiplied by coordination for tick!
    set load max-pxcor * (1 + random 30)
    ; Consumption per consumer (or family members), it is in range 0.666 ... 4
    set consumption 1 / (1 + random 3)
    set stock-resource (resource-home-initial / 100) * (consumption * load)
    set panic false
  ]


  set needs sum [consumption] of consumers

  create-shops shops-count [
    set color yellow
    set size 5
    set xcor random-xcor
    set ycor random-ycor

    set stock-resources (resource-shops-initial / 100 * needs) / shops-count
  ]

  reset-ticks
end

to go
  ; SUPLY RESOURCES
  ask shops [
    let supply (resource-recovery / 100 * needs / shops-count)
    set stock-resources  stock-resources + (supply - 3 + random 6)
  ]

  ; CONSUMERS SPENT RESOURCES
  ask consumers [

    ; consumer can achieve panic from someone else which forced him to go shopping earlier
    let penalty 0
    if panic [ set penalty 3 * consumption ]

    ifelse (stock-resource - consumption - penalty > 0) [
      set stock-resource (stock-resource - consumption)
      ifelse ( abs(xcor - home-x) > threshold or abs(ycor - home-y) > threshold) [ goHome ] [set color brown ]
    ] [
      goShopping
      ifelse panic [set color red][set color black]
    ]
  ]

  ; STOP CONDITIONS
  if (count shops with [ stock-resources < 2 ] / shops-count > 0.33) and (count consumers with [ panic = true ] / consumers-count > 0.95) [ stop ]
  tick
end

to goShopping
  let shop min-one-of shops [ distance myself ]
  let dist distance shop
  facexy ( [xcor] of shop) ( [ycor] of shop)
  ifelse dist < 1 [
      ask shop [
      ; kant-altruism - chance of being kant and take just half resources
      let kant-altruism 1
      if random 100 > nash-players [ set kant-altruism 0.5 ]
      ; panic-hoarding
      let hoarding 1
      if [panic and hoarding-if-panic] of myself = true [ set hoarding 1.5 ]
      ; pigouvian-tax - enables market restriction on loads (increase costs of resource)
      let consumption_ (([load] of myself * (1 - pigouvian-tax / 100) * kant-altruism * hoarding) * [consumption] of myself)
      ifelse (stock-resources >=  consumption_) [
        set stock-resources  (stock-resources - consumption_)
        ask myself [ set stock-resource  consumption_
          set panic false
        ]
        ]
        [ ask myself [
          set panic true
        ; infect other person being panic
          if random 100 < consumers-panic-of-lacking-resource and any? consumers with [panic = false] [
            ask  n-of 1 (consumers with [panic = false]) [
              set panic true
            ]
          ]
        ; altruisticly decreasing load in need
         if altruism-if-panic [ set load load * 0.99]
         ]
        ]

      ]
  ] [ fd 1 ]
end


to goHome
  facexy home-x home-y
  fd 1
end
@#$#@#$#@
GRAPHICS-WINDOW
274
10
753
490
-1
-1
7.7213115
1
10
1
1
1
0
1
1
1
-30
30
-30
30
0
0
1
ticks
30.0

SLIDER
18
144
193
177
consumers-count
consumers-count
1
1000
200.0
10
1
pop
HORIZONTAL

SLIDER
20
330
255
363
resource-recovery
resource-recovery
0
200
90.0
5
1
%
HORIZONTAL

SLIDER
25
564
481
597
consumers-panic-of-lacking-resource
consumers-panic-of-lacking-resource
0
20
1.0
1
1
% probability of influence other to become panic 
HORIZONTAL

SLIDER
20
191
192
224
shops-count
shops-count
0
10
3.0
1
1
pop
HORIZONTAL

TEXTBOX
11
10
161
136
Tragedy of Common:\n\n   P1/P2    full half\n   full     (2,2  4,1)\n   half    (1,4  3,3)\n\nPlayers:\n   Nash - full\n   Kant - half
11
12.0
0

BUTTON
149
51
212
84
NIL
go
NIL
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

BUTTON
148
10
211
43
NIL
setup
NIL
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

BUTTON
149
95
212
128
NIL
go
T
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

SLIDER
20
235
201
268
resource-home-initial
resource-home-initial
0
300
100.0
10
1
%
HORIZONTAL

PLOT
769
65
1002
185
Consumers-Panic
NIL
NIL
0.0
10.0
0.0
100.0
true
false
"" ""
PENS
"default" 1.0 0 -5298144 true "" "plot (count consumers with [ panic = true ]) / consumers-count * 100"

PLOT
770
325
1003
445
Consumers-Stock
NIL
NIL
0.0
10.0
0.0
10.0
true
false
"" ""
PENS
"Stock" 1.0 0 -8330359 true "" "plot count consumers with [ stock-resource > 2 * consumption ] "
"Without stock" 1.0 0 -16514813 true "" "plot count consumers with [ stock-resource < 2 * consumption ] "

SLIDER
20
283
204
316
resource-shops-initial
resource-shops-initial
0
500
0.0
10
1
%
HORIZONTAL

PLOT
769
455
1004
579
Shops-Stock
NIL
NIL
0.0
10.0
0.0
10.0
true
false
"" ""
PENS
"Stock" 1.0 0 -5298144 true "" "plot mean [ stock-resources ] of shops"
"Minimum" 1.0 0 -7500403 true "" "plot min [ stock-resources ] of shops"
"Maximum" 1.0 0 -14439633 true "" "plot max [ stock-resources ] of shops"

MONITOR
770
10
959
55
NIL
consumers with [ panic = true ]
17
1
11

SLIDER
23
476
195
509
pigouvian-tax
pigouvian-tax
0
80
5.0
5
1
%
HORIZONTAL

SLIDER
23
521
315
554
nash-players
nash-players
0
100
0.0
1
1
% Nash vs Kant
HORIZONTAL

SWITCH
23
432
202
465
altruism-if-panic
altruism-if-panic
1
1
-1000

PLOT
771
194
1002
314
Consumers-Load
NIL
NIL
0.0
10.0
0.0
10.0
true
false
"" ""
PENS
"default" 1.0 0 -10873583 true "" "plot mean [load] of consumers"

SWITCH
23
391
172
424
hoarding-if-panic
hoarding-if-panic
0
1
-1000

@#$#@#$#@
## WHAT IS IT?

Tragedy of the commons with supplementary panic spread


## HOW IT WORKS

The project is implemented in Netlogo and applies all mentioned attributes in global state or two breeds – Shop (cross) and Consumer (person). 

A shop has some resource capacity, which is restored per tick. Restore computation is equal to all consumers’ consumption and divided among all initialized shops. As mentioned, consumers are generated with random consumption (exhausted per tick) and random load (stocking limit at home, positioned on exact coordination, brown person). If loaded resources are not enough to fulfill consumption (which can be enlarged by panic), the consumer heads to nearest shop (black person).

In the shop, the consumer is evaluated as Kantian or Nash and requested its need (consumption multiplied by load and affect by hoarding or altruism), which is submitted or declined. Then the consumer returns to home or waits at the show, meanwhile naively spreading panic to other random consumers.

This is repeating until 2/3 shops are stocked and panicking consumers (red person) are less than 95 %. Model offers setting population of consumers and shops, which are limited as only one source for nearest consumers.


## THINGS TO NOTICE

Pigouvian tax and Altruism affects load, shop recovery and stops panic.

## EXTENDING THE MODEL

Way to and from shop should be considered in loads.
Shop can be switched by customer.
@#$#@#$#@
default
true
0
Polygon -7500403 true true 150 5 40 250 150 205 260 250

airplane
true
0
Polygon -7500403 true true 150 0 135 15 120 60 120 105 15 165 15 195 120 180 135 240 105 270 120 285 150 270 180 285 210 270 165 240 180 180 285 195 285 165 180 105 180 60 165 15

arrow
true
0
Polygon -7500403 true true 150 0 0 150 105 150 105 293 195 293 195 150 300 150

box
false
0
Polygon -7500403 true true 150 285 285 225 285 75 150 135
Polygon -7500403 true true 150 135 15 75 150 15 285 75
Polygon -7500403 true true 15 75 15 225 150 285 150 135
Line -16777216 false 150 285 150 135
Line -16777216 false 150 135 15 75
Line -16777216 false 150 135 285 75

bug
true
0
Circle -7500403 true true 96 182 108
Circle -7500403 true true 110 127 80
Circle -7500403 true true 110 75 80
Line -7500403 true 150 100 80 30
Line -7500403 true 150 100 220 30

butterfly
true
0
Polygon -7500403 true true 150 165 209 199 225 225 225 255 195 270 165 255 150 240
Polygon -7500403 true true 150 165 89 198 75 225 75 255 105 270 135 255 150 240
Polygon -7500403 true true 139 148 100 105 55 90 25 90 10 105 10 135 25 180 40 195 85 194 139 163
Polygon -7500403 true true 162 150 200 105 245 90 275 90 290 105 290 135 275 180 260 195 215 195 162 165
Polygon -16777216 true false 150 255 135 225 120 150 135 120 150 105 165 120 180 150 165 225
Circle -16777216 true false 135 90 30
Line -16777216 false 150 105 195 60
Line -16777216 false 150 105 105 60

car
false
0
Polygon -7500403 true true 300 180 279 164 261 144 240 135 226 132 213 106 203 84 185 63 159 50 135 50 75 60 0 150 0 165 0 225 300 225 300 180
Circle -16777216 true false 180 180 90
Circle -16777216 true false 30 180 90
Polygon -16777216 true false 162 80 132 78 134 135 209 135 194 105 189 96 180 89
Circle -7500403 true true 47 195 58
Circle -7500403 true true 195 195 58

circle
false
0
Circle -7500403 true true 0 0 300

circle 2
false
0
Circle -7500403 true true 0 0 300
Circle -16777216 true false 30 30 240

cow
false
0
Polygon -7500403 true true 200 193 197 249 179 249 177 196 166 187 140 189 93 191 78 179 72 211 49 209 48 181 37 149 25 120 25 89 45 72 103 84 179 75 198 76 252 64 272 81 293 103 285 121 255 121 242 118 224 167
Polygon -7500403 true true 73 210 86 251 62 249 48 208
Polygon -7500403 true true 25 114 16 195 9 204 23 213 25 200 39 123

cylinder
false
0
Circle -7500403 true true 0 0 300

dot
false
0
Circle -7500403 true true 90 90 120

face happy
false
0
Circle -7500403 true true 8 8 285
Circle -16777216 true false 60 75 60
Circle -16777216 true false 180 75 60
Polygon -16777216 true false 150 255 90 239 62 213 47 191 67 179 90 203 109 218 150 225 192 218 210 203 227 181 251 194 236 217 212 240

face neutral
false
0
Circle -7500403 true true 8 7 285
Circle -16777216 true false 60 75 60
Circle -16777216 true false 180 75 60
Rectangle -16777216 true false 60 195 240 225

face sad
false
0
Circle -7500403 true true 8 8 285
Circle -16777216 true false 60 75 60
Circle -16777216 true false 180 75 60
Polygon -16777216 true false 150 168 90 184 62 210 47 232 67 244 90 220 109 205 150 198 192 205 210 220 227 242 251 229 236 206 212 183

fish
false
0
Polygon -1 true false 44 131 21 87 15 86 0 120 15 150 0 180 13 214 20 212 45 166
Polygon -1 true false 135 195 119 235 95 218 76 210 46 204 60 165
Polygon -1 true false 75 45 83 77 71 103 86 114 166 78 135 60
Polygon -7500403 true true 30 136 151 77 226 81 280 119 292 146 292 160 287 170 270 195 195 210 151 212 30 166
Circle -16777216 true false 215 106 30

flag
false
0
Rectangle -7500403 true true 60 15 75 300
Polygon -7500403 true true 90 150 270 90 90 30
Line -7500403 true 75 135 90 135
Line -7500403 true 75 45 90 45

flower
false
0
Polygon -10899396 true false 135 120 165 165 180 210 180 240 150 300 165 300 195 240 195 195 165 135
Circle -7500403 true true 85 132 38
Circle -7500403 true true 130 147 38
Circle -7500403 true true 192 85 38
Circle -7500403 true true 85 40 38
Circle -7500403 true true 177 40 38
Circle -7500403 true true 177 132 38
Circle -7500403 true true 70 85 38
Circle -7500403 true true 130 25 38
Circle -7500403 true true 96 51 108
Circle -16777216 true false 113 68 74
Polygon -10899396 true false 189 233 219 188 249 173 279 188 234 218
Polygon -10899396 true false 180 255 150 210 105 210 75 240 135 240

house
false
0
Rectangle -7500403 true true 45 120 255 285
Rectangle -16777216 true false 120 210 180 285
Polygon -7500403 true true 15 120 150 15 285 120
Line -16777216 false 30 120 270 120

leaf
false
0
Polygon -7500403 true true 150 210 135 195 120 210 60 210 30 195 60 180 60 165 15 135 30 120 15 105 40 104 45 90 60 90 90 105 105 120 120 120 105 60 120 60 135 30 150 15 165 30 180 60 195 60 180 120 195 120 210 105 240 90 255 90 263 104 285 105 270 120 285 135 240 165 240 180 270 195 240 210 180 210 165 195
Polygon -7500403 true true 135 195 135 240 120 255 105 255 105 285 135 285 165 240 165 195

line
true
0
Line -7500403 true 150 0 150 300

line half
true
0
Line -7500403 true 150 0 150 150

pentagon
false
0
Polygon -7500403 true true 150 15 15 120 60 285 240 285 285 120

person
false
0
Circle -7500403 true true 110 5 80
Polygon -7500403 true true 105 90 120 195 90 285 105 300 135 300 150 225 165 300 195 300 210 285 180 195 195 90
Rectangle -7500403 true true 127 79 172 94
Polygon -7500403 true true 195 90 240 150 225 180 165 105
Polygon -7500403 true true 105 90 60 150 75 180 135 105

plant
false
0
Rectangle -7500403 true true 135 90 165 300
Polygon -7500403 true true 135 255 90 210 45 195 75 255 135 285
Polygon -7500403 true true 165 255 210 210 255 195 225 255 165 285
Polygon -7500403 true true 135 180 90 135 45 120 75 180 135 210
Polygon -7500403 true true 165 180 165 210 225 180 255 120 210 135
Polygon -7500403 true true 135 105 90 60 45 45 75 105 135 135
Polygon -7500403 true true 165 105 165 135 225 105 255 45 210 60
Polygon -7500403 true true 135 90 120 45 150 15 180 45 165 90

sheep
false
15
Circle -1 true true 203 65 88
Circle -1 true true 70 65 162
Circle -1 true true 150 105 120
Polygon -7500403 true false 218 120 240 165 255 165 278 120
Circle -7500403 true false 214 72 67
Rectangle -1 true true 164 223 179 298
Polygon -1 true true 45 285 30 285 30 240 15 195 45 210
Circle -1 true true 3 83 150
Rectangle -1 true true 65 221 80 296
Polygon -1 true true 195 285 210 285 210 240 240 210 195 210
Polygon -7500403 true false 276 85 285 105 302 99 294 83
Polygon -7500403 true false 219 85 210 105 193 99 201 83

square
false
0
Rectangle -7500403 true true 30 30 270 270

square 2
false
0
Rectangle -7500403 true true 30 30 270 270
Rectangle -16777216 true false 60 60 240 240

star
false
0
Polygon -7500403 true true 151 1 185 108 298 108 207 175 242 282 151 216 59 282 94 175 3 108 116 108

target
false
0
Circle -7500403 true true 0 0 300
Circle -16777216 true false 30 30 240
Circle -7500403 true true 60 60 180
Circle -16777216 true false 90 90 120
Circle -7500403 true true 120 120 60

tree
false
0
Circle -7500403 true true 118 3 94
Rectangle -6459832 true false 120 195 180 300
Circle -7500403 true true 65 21 108
Circle -7500403 true true 116 41 127
Circle -7500403 true true 45 90 120
Circle -7500403 true true 104 74 152

triangle
false
0
Polygon -7500403 true true 150 30 15 255 285 255

triangle 2
false
0
Polygon -7500403 true true 150 30 15 255 285 255
Polygon -16777216 true false 151 99 225 223 75 224

truck
false
0
Rectangle -7500403 true true 4 45 195 187
Polygon -7500403 true true 296 193 296 150 259 134 244 104 208 104 207 194
Rectangle -1 true false 195 60 195 105
Polygon -16777216 true false 238 112 252 141 219 141 218 112
Circle -16777216 true false 234 174 42
Rectangle -7500403 true true 181 185 214 194
Circle -16777216 true false 144 174 42
Circle -16777216 true false 24 174 42
Circle -7500403 false true 24 174 42
Circle -7500403 false true 144 174 42
Circle -7500403 false true 234 174 42

turtle
true
0
Polygon -10899396 true false 215 204 240 233 246 254 228 266 215 252 193 210
Polygon -10899396 true false 195 90 225 75 245 75 260 89 269 108 261 124 240 105 225 105 210 105
Polygon -10899396 true false 105 90 75 75 55 75 40 89 31 108 39 124 60 105 75 105 90 105
Polygon -10899396 true false 132 85 134 64 107 51 108 17 150 2 192 18 192 52 169 65 172 87
Polygon -10899396 true false 85 204 60 233 54 254 72 266 85 252 107 210
Polygon -7500403 true true 119 75 179 75 209 101 224 135 220 225 175 261 128 261 81 224 74 135 88 99

wheel
false
0
Circle -7500403 true true 3 3 294
Circle -16777216 true false 30 30 240
Line -7500403 true 150 285 150 15
Line -7500403 true 15 150 285 150
Circle -7500403 true true 120 120 60
Line -7500403 true 216 40 79 269
Line -7500403 true 40 84 269 221
Line -7500403 true 40 216 269 79
Line -7500403 true 84 40 221 269

wolf
false
0
Polygon -16777216 true false 253 133 245 131 245 133
Polygon -7500403 true true 2 194 13 197 30 191 38 193 38 205 20 226 20 257 27 265 38 266 40 260 31 253 31 230 60 206 68 198 75 209 66 228 65 243 82 261 84 268 100 267 103 261 77 239 79 231 100 207 98 196 119 201 143 202 160 195 166 210 172 213 173 238 167 251 160 248 154 265 169 264 178 247 186 240 198 260 200 271 217 271 219 262 207 258 195 230 192 198 210 184 227 164 242 144 259 145 284 151 277 141 293 140 299 134 297 127 273 119 270 105
Polygon -7500403 true true -1 195 14 180 36 166 40 153 53 140 82 131 134 133 159 126 188 115 227 108 236 102 238 98 268 86 269 92 281 87 269 103 269 113

x
false
0
Polygon -7500403 true true 270 75 225 30 30 225 75 270
Polygon -7500403 true true 30 75 75 30 270 225 225 270
@#$#@#$#@
NetLogo 6.0.4
@#$#@#$#@
@#$#@#$#@
@#$#@#$#@
<experiments>
  <experiment name="Kant-vs-Nash" repetitions="10" sequentialRunOrder="false" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <timeLimit steps="5000"/>
    <metric>count consumers with [ panic = true ] / consumers-count * 100</metric>
    <metric>mean [load] of consumers</metric>
    <metric>count consumers with [ stock-resource &lt; 2 * consumption ]</metric>
    <metric>mean [ stock-resources ] of shops</metric>
    <enumeratedValueSet variable="resource-shops-initial">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="altruism-if-panic">
      <value value="false"/>
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="pigouvian-tax">
      <value value="0"/>
      <value value="10"/>
      <value value="5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="consumers-panic-of-lacking-resource">
      <value value="1"/>
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="shops-count">
      <value value="1"/>
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="nash-players">
      <value value="100"/>
      <value value="50"/>
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="resource-home-initial">
      <value value="50"/>
      <value value="100"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="hoarding-if-panic">
      <value value="false"/>
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="consumers-count">
      <value value="200"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="resource-recovery">
      <value value="70"/>
      <value value="80"/>
      <value value="90"/>
    </enumeratedValueSet>
  </experiment>
</experiments>
@#$#@#$#@
@#$#@#$#@
default
0.0
-0.2 0 0.0 1.0
0.0 1 1.0 0.0
0.2 0 0.0 1.0
link direction
true
0
Line -7500403 true 150 150 90 180
Line -7500403 true 150 150 210 180
@#$#@#$#@
0
@#$#@#$#@
