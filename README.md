## About

In this paper, tragedy of the commons is salted with
panic spreading through consumers, observing influences on
already discovered mechanics. Following actual research, the
new model was implemented in Netlogo, described and
experimentally tested.


![Preview](https://i.imgur.com/fYHJJyT.png)